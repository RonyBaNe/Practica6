using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Api.Responses;
using Entities;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;

namespace Api.Authorization;

[AttributeUsage(AttributeTargets.Class | AttributeTargets.Method)]
public class AuthorizeAttribute : Attribute, IAuthorizationFilter
{
    public void OnAuthorization(AuthorizationFilterContext context)
    {

        var user = (User) context.HttpContext.Items["user"];

        if (user != null) return;

        var response = new Response<string>();
        context.Result = new JsonResult(response){
            StatusCode = StatusCodes.Status401Unauthorized,
        };
    }
}
