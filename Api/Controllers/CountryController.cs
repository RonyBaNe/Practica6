using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;
using Api.Responses;
using Entities;
using Api.Services;
using Api.Services.Interfaces;
using Api.Authorization;

namespace Api.Controllers;

[AuthorizeAttribute]
[ApiController]
public class CountryController : ControllerBase
{
    private readonly ICountryService _countryService;
    public CountryController(ICountryService countryService)
    {
        _countryService = countryService;
    }


    [HttpGet]
    [Route("api/[controller]/{countryCode}/states")]
    public async Task<ActionResult<Response<IEnumerable<State>>>> GetStateByCountryCodeAsync(string countryCode)
    {
        var response = new Response<IEnumerable<State>>();

        var states = await _countryService.GetStateByCountryCodeAsync(countryCode);
        
        response.Data = states;

        return Ok(response);
    
    }

}
