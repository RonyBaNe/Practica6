using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Entities;
using Entities.Dto;
using Entities.Http;

namespace Api.Repositories.Interfaces
{
    public interface ICustomerRepository
    {
        Task<List<Customer>> GetAllAsync();
        Task<Customer> GetCustomerByIdAsync(int Id);
        Task<Customer> SaveAsync(Customer customer);
        Task<Customer> UpdateAsync(Customer customer);
        Task<bool> DeleteAsync(int Id);
        Task<List<CustomerDto>> SearchAsync(CustomerFilter filter);
        Task<List<CustomerDto>> SearchAsync(StateFilter filter);
    }
}