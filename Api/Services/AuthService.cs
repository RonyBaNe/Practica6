using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Api.Services.Interfaces;
using Entities;
using Microsoft.IdentityModel.Tokens;

namespace Api.Services
{
    public class AuthService: IAuthService
    {
        public async Task<UserSession>  GetAuthorization(AuthEnt auth)
        {
            var userSession = new UserSession();
            var user = new User();

            if (auth.UserName != null && auth.Password != null)
            {
                userSession.UserId = user.Id;
                userSession.Token = GenerateJwtToken(userSession);
            } 
            return userSession;
        }



        private string GenerateJwtToken(UserSession session)
        {

            var secret = "THIS IS USED TO SIGN AND VERIFY JWT TOKENS, REPLACTE IT WITH YOUR OWN SECRET, IT CAN BE ANY STRING";
            var tokenHandler = new JwtSecurityTokenHandler();

            var key = System.Text.Encoding.ASCII.GetBytes(secret);

            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(new[] { new Claim("id", session.UserId.ToString()) }),
                Expires = System.DateTime.UtcNow.AddDays(7),
                SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature)

            };

            var token = tokenHandler.CreateToken(tokenDescriptor);

            return tokenHandler.WriteToken(token);
        }


        public async Task<User> GetUserById(int id)
        {
            var user = new User();



            if (id != 0)
            {
                user = new User{Id = user.Id, FirstName = user.FirstName, LastName = user.LastName, UserName = user.UserName, Password = user.Password, ProfileId = user.ProfileId};
            }
            return user;

            /*if(id == 1)
            {
                user = new User{Id = 1, FirstName = "gibran", LastName = "gamiño"};
            }
            return user;*/
        }
    }
}