using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Entities;

namespace Api.Services.Interfaces;
    public interface IAuthService
    {
        Task<UserSession> GetAuthorization(AuthEnt auth);

        Task<User> GetUserById(int id);
    }
