using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Entities.Http
{
    public class StateFilter:BaseFilter
    {
        public string? State {get; set;}

        protected override void SetParameters()
        {
            base.SetParameters();
            _parameters["State"] = State;
        }
    }
}