using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
//Prueba unitaria
using Moq;
using Xunit;
using Api.Repositories.Interfaces;
using Api.Services;
using Entities;

namespace Test.ApiServices
{
    public class CustomerServiceTest
    {
        [Fact]
        public async Task GetAllAsync_WhenGetList_ReturnList()
        {
            //arrange
            var expected = new List<Customer>
            {
                new Customer{Id = 1,Name = "Customer 1"},
                new Customer{Id = 2, Name = "Cusomer 2"}
                
            };
            var mock = new Mock<ICustomerRepository>();

            mock.Setup(n => n.GetAllAsync()).Returns(Task.FromResult(expected));
                
            var sut = new CustomerService(mock.Object);


            //var repo = new CustomerRepository();

            //act
            var result = await sut.GetAllAsync();

            //asset

            Assert.Equal(expected,result);
        }
        [Fact]
        public async Task GetNameAsync_WhenGetList_ReturnList()
        {
            //arrange
            var expected = new List<Customer>
            {
                new Customer{Name = "Customer"},
                new Customer{Name = "Customer2"} 
            };

            var mock = new Mock<ICustomerRepository>();

            mock.Setup(n => n.GetAllAsync()).Returns(Task.FromResult(expected));
            var sut = new CustomerService(mock.Object);

            //act
            var resultado = await sut.GetAllAsync();

            //asset
            Assert.Equal(expected, resultado);
        }
        [Fact]
        public async Task GetRFCAsync_WhenGetList_ReturnList()
        {
            //arrange
            var expected = new List<Customer>
            {
                new Customer{RFC = "1234"},
                new Customer{RFC = "12345"} 
            };

            var mock = new Mock<ICustomerRepository>();

            mock.Setup(n => n.GetAllAsync()).Returns(Task.FromResult(expected));
            var sut = new CustomerService(mock.Object);

            //act
            var resultado = await sut.GetAllAsync();

            //asset
            Assert.Equal(expected, resultado);
        }
        [Fact]
        public async Task GetEmailAsync_WhenGetList_ReturnList()
        {
            //arrange
            var expected = new List<Customer>
            {
                new Customer{Email = "hh@gmail.com"},
                new Customer{Email = "gg@gmail.com"} 
            };

            var mock = new Mock<ICustomerRepository>();

            mock.Setup(n => n.GetAllAsync()).Returns(Task.FromResult(expected));
            var sut = new CustomerService(mock.Object);

            //act
            var resultado = await sut.GetAllAsync();

            //asset
            Assert.Equal(expected, resultado);
        }
        [Fact]
        public async Task GetPhoneAsync_WhenGetList_ReturnList()
        {
            //arrange
            var expected = new List<Customer>
            {
                new Customer{Phone = "503-528-6657"},
                new Customer{Phone = "606-614-1687"}
            };

            var mock = new Mock<ICustomerRepository>();

            mock.Setup(n => n.GetAllAsync()).Returns(Task.FromResult(expected));
            var sut = new CustomerService(mock.Object);

            //act
            var resultado = await sut.GetAllAsync();

            //asset
            Assert.Equal(expected, resultado);
        }
       
    }  


    public class Practica3Test
    {
         [Fact]
        public async Task UpdateAsync_WhenUpdateCustomer_ReturnSameCustomer()
        {
            //arrange
            var customer = new Customer{Id = 1};
            var mock = new Mock<ICustomerRepository>();

            mock.Setup(m => m.UpdateAsync(customer)).Returns(Task.FromResult(customer));

            var sut = new CustomerService(mock.Object);

            //act
            var result = await sut.UpdateAsync(customer);

            //assert
            Assert.Equal(customer, result);
        }
    
         [Fact]
        public async Task SaveAsync_WhenSaveCustomer_ReturnCustomerWithIdGreaterThanZero()
        {
            //arrange
            var customer = new Customer{Id = 0};
            var expected = new Customer{ Id = 1};

            var mock = new Mock<ICustomerRepository>();

            mock.Setup( m => m.SaveAsync(customer)).Returns(Task.FromResult(expected));

            var sut = new CustomerService(mock.Object);

            //act
            customer = await sut.SaveAsync(customer);

            //assert
            Assert.Equal(expected.Id, customer.Id);
        }
        
        
        [Fact]
        public async Task DeleteAsync_WhenDeleteFails_ReturnFalse()
        {
            //arrange
            var expected = false;
            var customer = new Customer{Id = 1};

            var mock = new Mock<ICustomerRepository>();

            mock.Setup( m => m.DeleteAsync(customer.Id)).Returns(Task.FromResult(expected));

            var sut = new CustomerService(mock.Object);

            //act
            var result = await sut.DeleteAsync(customer.Id);
            //assert
            Assert.Equal(expected, result);
        }

        [Fact]
        public async Task DeleteAsync_WhenDeleteFails_ReturnTrue()
        {
            //arrange
            var expected = true;
            var customer = new Customer{Id = 1};

            var mock = new Mock<ICustomerRepository>();

            mock.Setup( m => m.DeleteAsync(customer.Id)).Returns(Task.FromResult(expected));

            var sut = new CustomerService(mock.Object);

            //act
            var result = await sut.DeleteAsync(customer.Id);
            //assert
            Assert.Equal(expected, result);
        }


        [Fact]
        public async Task GetCustomerByIdAsync_WhenGetCustomer_ReturnsCustomer()
        {
            //arrange
            var id = 1;
            var expected = new Customer{Id = id};
            var mock = new Mock<ICustomerRepository>();

            mock.Setup(x => x.GetCustomerByIdAsync(id)).Returns(Task.FromResult(expected));

            var sut = new CustomerService(mock.Object);
            //act
            var result = await sut.GetCustomerByIdAsync(id);

            //assert
            Assert.Equal(expected, result);
        }
    }
}