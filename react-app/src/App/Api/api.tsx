import axios, {AxiosResponse} from 'axios'
import { idText } from 'typescript'

import ICustomer from '../Entities/customer'
import ICustomerFilter from '../Entities/http/customerFilter'
import FilterbyState from '../Entities/http/FilterbyState'
import CustomerStore from '../Store/customerStore'

axios.defaults.baseURL = 'https://localhost:7181/api'

const responseBody = (response: AxiosResponse) =>  response.data

const request = {
    get: (url: string, filters:{} = {}) => {
        const params = new URLSearchParams(filters)
        return axios.get(url, {params}).then(responseBody)
    },

    post: (url: string, body: {}) => axios.post(url, body).then(responseBody),
    
    put: (url: string, body: {}) => axios.put(url, body).then(responseBody),

    delete: (url: string, body: {}) => axios.delete(url, body).then(responseBody)
}

const Customer = {
    list: () => request.get('customer'),

    search : (filters:ICustomerFilter) => request.get('customer/search',filters),

    searchbystate : (filters:FilterbyState) => request.get('customer/searchbystate',filters),
    
    create: (customer: ICustomer) => request.post('customer',customer),
    
    update: (customer: ICustomer) => request.put('customer',customer),

    delete: (customer: ICustomer) => request.delete('customer/',customer.id)
}

const Country = {
    listStateByCountry : (countryCode:string) => request.get(`country/${countryCode}/states`)
}

const api ={
    axios,
    Customer,
    Country
}

export default api;