import React from 'react'
import { observe } from 'mobx';
import { observer } from 'mobx-react-lite';


import CustomerList from './CustomerList';
import CustomerEdit from './CustomerEdit';
import ICustomer from '../../Entities/customer';
import { store, useStore } from '../../Store/store';

const CustomersDashboard = () => {


    const {customerStore} = useStore()
    const {editCustomer, isListLoaded} = customerStore

    return(

        <React.Fragment>

            {
                editCustomer === false && isListLoaded === true &&
                <CustomerList/>
            }
            {
                editCustomer && isListLoaded === true && 
                <CustomerEdit/>
            }
            {
                isListLoaded === false && <div>Loading</div>
            }
        </React.Fragment>
    )
}

export default observer(CustomersDashboard);